# List of script ideas

scriptCode = []

import random, os, glob, sys
from time import sleep

# Should be used to find mods all the time v
def FindMyMods(Name):
    for root, dirs, files in os.walk('/'):
        if Name in dirs:
            return os.path.abspath(os.path.join(root, Name))

    return None

MyModsPath = FindMyMods("MyModules")

if MyModsPath is not None:
    sys.path.append(MyModsPath)
    from MyMods import clear, editLine, addLine

    
os.chdir(os.path.dirname(os.path.abspath(__file__)))

path = "scripts/"
files = glob.glob(os.path.join(path, "*.py"))
scripts = [os.path.basename(f) for f in files]
print(scripts)


end = len(scripts)
script = random.randint(0, end-1)
script = scripts[script] # Choose a random script
# Puts code into a list to have it be typed out with numbers next to the lines.
with open(f"scripts/{script}") as f:
    for line in f:
        scriptCode.append(line)

# Might add backgrounds for each script

def main():
    clr = "a"
    while True:
        if clr != "n":
            clear()
        choice = input("Do you want to:\nRun = Run the script and see if it works as intended\nCode = Look at the code to look for errors\nLeave = Stop helping the student\nEdit = Edit the student's code\n\n").lower()

        if choice == "run":
            with open(f"scripts/{script}") as f:
                code = f.read()
            try:
                exec(code)
                clr = input("The code has finished running. Clear the terminal? Y or N\n").lower
                if clr == "y":
                    clear()
                if clr == "n":
                    print("\n")
            except IndexError or ValueError or SyntaxError or TabError or TypeError:
                print("\nThere was an error! Check the code to see the problem.")
                sleep(2)
            clear()
    
        elif choice == "code":
            a = 1
            with open(f"scripts/{script}") as f:
                for line in f:
                    print(f"{a}.   {line}", end= "")
                    a += 1
            clr = input("\nDo you want to clear the terminal? Y or N\n").lower()
            if clr == "y":
                clear()
            if clr == "n":
                print("\n")

        elif choice == "edit":
            which = input("Do you want to add a new line or change a line? Add or Change\n").lower()
            if which == "add":
                lineNum = int(input("Where is the line you're adding? If the line you're adding is between two lines, type the number of the bottom one. "))
                addLine(script, lineNum, scriptCode, "scripts/")
            elif which == "change":
                lineNum = int(input("What line do you want to change? "))
                editLine(script, lineNum, scriptCode, "scripts/")
    
        elif choice == "leave":
            clear()
            exit()

main()
