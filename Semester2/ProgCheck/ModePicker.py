"""
You can be a teacher or a student.

If the user wants to be a student, the script that they have will be printed to show them.
They can choose to change a certain part of the code, run it, or submit it.
If they choose to submit, the code (actual script) would check if it meets everything in the rubric.
Depending on how much they get correct, the can get from an A to an F.

If the user wants to be a teacher, they would check the code and give the user a grade depending on how they did.
The teacher can be given a different script each time they run this file.
Some scripts might have a lot of errors while others have few to no errors.
Some scripts would be fine with the grade their given while others won't.
Some would fix themselves and have the teacher check again while others won't resubmit.

At the end of both, I could give them a score depending on how good they were at grading assingments.
"""

# By: 0xDarkstar
# Date: Start: Jan 31, 2023 End: Feb 9, 2023

import os, sys


def FindMyMods(Name):
    for root, dirs, files in os.walk('/'):
        if Name in dirs:
            return os.path.abspath(os.path.join(root, Name))

    return None

MyModsPath = FindMyMods("MyModules")

if MyModsPath is not None:
    sys.path.append(MyModsPath)
    from MyMods import clear

os.chdir(os.path.dirname(os.path.abspath(__file__)))

# Changes the directory that the terminal looks in when one of the files wants to look for another.
# EX: This file can call two other files ("proofreader.py" and "teacher.py"). If the above code wasn't there and you were to run this file from /home/ by going "python (path to file)" then this file would run, but it would search for the others in /home/ instead of "ProgCheck/" and not work because those files aren't there (unless).

def main():
    clear()
    print("Hello! There are two modes for you to choose. There is a 'proofreader' mode and 'teacher' mode.\n\nProofreader mode is a feature in which users check scripts for errors and provide feedback on any issues they find. The goal of proofreader mode is to ensure that scripts are accurate, free of errors, and ready for use.\n\nTeacher mode is a feature that allows users to evaluate and grade scripts according to a pre-defined rubric. The goal of teacher mode is to provide objective and standardized assessments of the scripts, based on specific criteria such as code quality, accuracy, efficiency, and adherence to best practices.")

    while True:
        mode = input("\nDo you want to be the proofreader or the teacher? P or T\n").lower()

        if mode == "proofreader" or mode == "p":
            filename = "proofreader.py"
            clear()
            break
        elif mode == "teacher" or mode == "t":
            filename = "teacher.py"
            clear()
            break
        else:
            print("You didn't input a choice.")
            clear()


    # Use the code below to print the scripts
    '''
    a = 1
    with open(filename) as f:
        for line in f:
            print(f"{a}.   {line}")
            a += 1
    '''

    # Code below is used to run scripts
    with open(filename) as f:
        code = f.read()

    exec(code)
    clear()
main()
