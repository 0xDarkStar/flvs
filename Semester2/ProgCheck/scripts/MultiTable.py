# This program will create a multiplication table for any range that the user gives.

from tabulate import tabulate # To make a table

print("This program will make a multiplication table for you, just choose the upper limit")
upperLim = int(input("What would the upper limit of the table be?\n"))

MultiTable = list(list(range(1*i,(upperLim+1)*i, i)) for i in range(1,upperLim+1)) # Makes a multiplication table by having a list of lists with every number on the table.
print(tabulate(MultiTable, headers='firstrow', tablefmt='fancy_grid')) # Makes the table the user sees