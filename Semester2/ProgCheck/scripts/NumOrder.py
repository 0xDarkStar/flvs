# This program will order any numbers given into greatest to least or least to greatest, depending on what the user wants.

nums = []

def main():
    print("I'm here to help you order all the numbers you give me!")
    amount = int(input("How many numbers do you want me to sort?\n"))

    for a in range(amount):
        if a == 0:
            number = int(input("What is the first number?\n"))
            nums.append(number)
        else:
            number = int(input("What is the next number?\n"))
            nums.append(number)
    
    print("Your numbers in order are:\n" + ", ".join(nums))
main()
