# This script can be used to find if the number given is a prime number

print("I'll help you find out if any number you give me is prime.")
num = int(input("What is the number that you want to check?\n"))

# If given number is greater than 1
if num > 1:
    # Iterate from 2 to n / 2
    for i in range(2, int(num/2)+1):
        # If num is divisible by any number between
        # 2 and n / 2, it is not prime
        if (num % i) == 0:
            print(num, "is not a prime number")
            break
    else:
        print(num, "is a prime number")
else:
    print(num, "is not a prime number")

'''
This is from:
https://www.geeksforgeeks.org/python-program-to-check-whether-a-number-is-prime-or-not/
'''