# To find the radius of any circle!
# No matter how big or how small, the radius will be found!
# Using either the diameter or the circumference!

import platform, os
from time import sleep
OS = platform.system()
def clear():
    if OS == "Linux" or OS == "macOs": # Find the operating system to be able to clear their terminal.
        os.system("clear")
    elif OS == "Windows":
        os.system("cls")
    else:
        print("What system are you using???")

def main():
    clear()
    print("I'll be finding the radius of a circle for you.")
    which = input("What do you have? Diameter (d) or circumference (c).\n").lower()

    if which == "c" or which == "circumference":
        clear()
        circ = float(input("What is the circumference of your circle? Numbers only, no ','.\n"))

        rad = (circ/3.14)/2

        print(f"Your radius is: {rad} or {int(rad)}")
        print("\n\nIt doesn't round btw...")
        sleep(4)
    elif which == "d" or which == "diameter":
        clear()
        diam = float(input("What is the diameter of your circle? Numbers only, no ','.\n"))

        rad = diam/2

        print(f"Your radius is: {rad} or {int(rad)}")
        print("\n\nIt doesn't round btw...")
        sleep(4)
    else:
        print("You didn't input one of the choices. Try again.")
        sleep(3)
        main()
main()