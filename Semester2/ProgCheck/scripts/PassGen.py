#Will create a password using random letters.
#If user prefers to have special characters or numbers, they will be added.

import random

#Lists
letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "w", "x", "y", "z"]
special = ["`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "=", "+", "[", "]", "{", "}", "\\", "|", ";", ":", ",", "<", ">", ".", "?", "/"]

print("This is a random password generator.")
count = int(input("How long do you want the password to be? Input a number.\n"))
illegal = input("If there are any characters you can't use, type them now.\n")

illegals = sorted(illegal)

password = ""
for a in range(count):
    lists = random.randint(0,2)
    if lists == 0:
        let = random.randint(0, 24)
        if letters[let] in illegals:
            pass
        else:
            password += letters[let]
    elif lists == 1:
        num = str(random.randint(0, 9))
        if num in illegals:
            pass
        else:
            password += num
    elif lists == 2:
        spec = random.randint(0, 29)
        if special[spec] in illegals:
            pass
        else:
            password += special[spec]

print(f"Your password is:\n{password}")