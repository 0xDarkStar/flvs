# A little rock paper scissors game made for fun.

import random, platform, os
from time import sleep

OS = platform.system()
def clear():
    if OS == "Linux" or OS == "macOs": # Find the operating system to be able to clear their terminal.
        os.system("clear")
    elif OS == "Windows":
        os.system("cls")
    else:
        print("What system are you using???")

choices = ["rock", "paper", "scissors"]

print("This is a simple rock paper scissors game in which you play rock paper scissors against a bot.")
games = int(input("How many games do you want to play?\n"))

for a in range(games):
    bot = random.randint(0, 2)
    bot = choices[bot] # Randomly chooses bot's play
    print(bot)
    sleep(1)
    clear()
    choice = input("Rock, paper, or scissors?\n").lower() # Player chooses what they want to play
    if choice not in choices:
        print("You misspelled your choice! Try again and make sure to spell it correctly. (As shown)")
        choice = input("Rock, paper, or scissors?")
    clear()
    print(f'''
    /----------------------\\
    |   User   |    Bot    |
    |----------|-----------|
    |   {choice[0].upper()}      |    {bot[0].upper()}      |
    \\----------------------/''')
    # Paper
    if choice == "paper":
        if bot == "paper":
            print("Nobody Wins! Its a tie.")
            sleep(1)
        elif bot == "rock":
            print("The Player Wins! Paper covers Rock.")
            sleep(1)
        elif bot == "scissors":
            print("The Bot Wins! Scissors cuts Paper.")
            sleep(1)
    # Rock
    elif choice == "rock":
        if bot == "paper":
            print("The Bot Wins! Paper covers Rock.")
            sleep(1)
        elif bot == "rock":
            print("Nobody Wins! Its a tie.")
            sleep(1)
        elif bot == "scissors":
            print("The Player Wins! Rock beats Scissors.")
            sleep(1)
    # Scissors
    elif choice == "scissors":
        if bot == "paper":
            print("The Player Wins! Scissors cuts Paper.")
            sleep(1)
        elif bot == "rock":
            print("The Bot Wins! Rock beats Scissors")
            sleep(1)
        elif bot == "scissors":
            print("Nobody Wins! Its a tie.")
            sleep(1)
    clear()

