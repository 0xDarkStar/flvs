# This is a simple calculator script.

# Date made: June 17, 2022

while True:
    types = input("""
Please type in the math operation you would like to complete:
+ for addition
- for subtraction
* for multiplication
/ for division
""")

    if types == "*":
        try:
            num1 = int(input("What is your first number? "))
        except ValueError:
            print("You put something other than a number.") # User didn't put a number, so they try again.
            continue
        try:
            num2 = int(input("What is your second number? "))
        except ValueError:
            print("You put something other than a number.")
            continue
        print(f"{num1} * {num2} = {num1 * num2}")

    elif types == "/":
        try:
            num1 = int(input("What is your first number? "))
        except ValueError:
            print("You put something other than a number.")
            continue
        try:
            num2 = int(input("What is your second number? "))
        except ValueError:
            print("You put something other than a number.")
            continue
        print(f"{num1} / {num2} = {num1 / num2}")

    elif types == "+":
        try:
            num1 = int(input("What is your first number? "))
        except ValueError:
            print("You put something other than a number.")
            continue
        try:
            num2 = int(input("What is your second number? "))
        except ValueError:
            print("You put something other than a number.")
            continue
        print(f"{num1} + {num2} = {num1 + num2}")

    elif types == "-":
        try:
            num1 = int(input("What is your first number? "))
        except ValueError:
            print("You put something other than a number.")
            continue
        try:
            num2 = int(input("What is your second number? "))
        except ValueError:
            print("You put something other than a number.")
            continue
        print(f"{num1} - {num2} = {num1 - num2}")
    
    cont = input("Do you wish to do more calculations? Y/n\n").lower()
    if cont == "n":
        break