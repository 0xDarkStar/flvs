# Allows the user to make To-Do lists quickly and easily!

import platform, os
from time import sleep
OS = platform.system()
def clear():
    if OS == "Linux" or OS == "macOs": # Find the operating system to be able to clear their terminal.
        os.system("clear")
    elif OS == "Windows":
        os.system("cls")
    else:
        print("What system are you using???")

tasksL = []

print("Hello! This script takes your inputs and puts them into an easy to read To-Do list.")
num = int(input("How many tasks would be in your To-Do list? Please input number.\n")) # How many tasks do they have?

for a in range(num): # They write down all their tasks
    clear()
    if a == 0:
        task = input("Please input a task.\n")
    else:
        task = input("Please input the next task.\n")
    tasksL.append(task)

clear()
print(tasksL)
print("Your To-Do list is:\n\n- " + "\n- ".join(tasksL))
sleep(5)