'''
For when you really need to find the tempurature in a different unit of measurement.

Made by Braith Spears

F to C
(F - 32) * 5/9 = C

C to F
(C * 9/5) + 32 = F

F to K
(F - 32) * 5/9 + 273.15 = K

K to F
(K - 273.15) * 9/5 + 32 = F

C to K
C + 273.15 = K

K to C
K - 273.15 = C
'''

import math
from time import sleep

# To check
UnitsOfMeasurement = ["f", "c", "k"]

units = input("What two units are you changing in between? (Format: 'f,c', first one being what you're changing from, second being what you want to change to)\n").lower()

if units[0] not in UnitsOfMeasurement or units[-1] not in UnitsOfMeasurement or units[0] == units[-1]:
    print("You didn't input two different units of measurement")
    sleep(2)

else:
    if units[0] == "f":
        while True:
            try:
                F = float(input("What is the tempurature in Fahrenheit? "))
            except ValueError:
                print("You didn't input a number")
            else:
                break
        if units[-1] == "c":
            C = (F - 32) * 5/9
            print(f"{F} degrees in Fahrenheit is {C} degrees in Celsius")
            sleep(2)
        elif units[-1] == "k":
            K = (F - 32) * 5/9 + 273.15
            print(f"{F} degrees in Fahrenheit is {K} degrees in Kelvin")
            sleep(2)

    if units[0] == "c":
        while True:
            try:
                C = float(input("What is the tempurature in Celsius? "))
            except ValueError:
                print("You didn't input a number")
            else:
                break
        if units[-1] == "f":
            F = (C * 9/5) + 32
            print(f"{C} degrees in Celsius is {F} degrees in Fahrenheit")
            sleep(2)
        elif units[-1] == "k":
            K = C + 273.15
            print(f"{C} degrees in Celsius is {K} degrees in Kelvin")
            sleep(2)

    if units[0] == "k":
        while True:
            try:
                K = float(input("What is the tempurature in Kelvin? "))
            except ValueError:
                print("You didn't input a number")
            else:
                break
        if units[-1] == "f":
            F = (K - 273.15) * 9/5 + 32
            print(f"{K} degrees in Kelvin is {F} degrees in Fahrenheit")
            sleep(2)
        elif units[-1] == "c":
            C = K - 273.15
            print(f"{K} degrees in Kelvin is {C} degrees in Celsius")
            sleep(2)