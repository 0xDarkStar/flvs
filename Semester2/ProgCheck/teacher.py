# List of script ideas

global scripts, readers

readers = ["teacher.py", "proofreader.py"]

import random, platform, os, glob
from time import sleep
from tabulate import tabulate

path = "scripts/"
files = glob.glob(os.path.join(path, "*.py"))
scripts = [os.path.basename(f) for f in files]


OS = platform.system()
def clear():
    if OS == "Linux" or OS == "macOs": # Find the operating system to be able to clear their terminal.
        os.system("clear")
    elif OS == "Windows":
        os.system("cls")
    else:
        print("What system are you using???")

global script

end = len(scripts)
script = random.randint(0, end)
script = scripts[script] # Randomize the chosen script
print(script)


rubric = [["Categories", "Excellent", "Good", "Needs Improvement"], 
          ["Software \nDevelopment \nLife Cycle", "21-30 pts.\nWork contains all required elements:\n   - planning and analysis is complete\n   - design includes pseudocode for input, decisions, and output\n   - coding clearly reflects pseudocode\n   - testing determines bugs and solutions, which are thoroughly explained\n   - maintenance includes thoughtful answers to each question", "11-20 pts.\nWork contains most required elements:\n   - planning and analysis is complete\n   - design includes pseudocode for input, decisions, and output\n   - coding clearly reflects pseudocode\n   - testing determines bugs and solutions, which are thoroughly explained\n   - maintenance includes thoughtful answers to each question", "0-10 pts.\nWork contains few required elements:\n   - planning and analysis is complete\n   - design includes pseudocode for input, decisions, and output\n   - coding clearly reflects pseudocode\n   - testing determines bugs and solutions, which are thoroughly explained\n   - maintenance includes thoughtful answers to each question"], 
          ["Program Design\n and Performance", "16-20 pts.\nWork contains all required elements\n   - programmer's name and the date\n   - comments explain the purpose of the program\n   - the program is correctly indented\n   - meaningful variable names are used\n   - the program runs correctly\n   - the program contains no syntax errors\n   - The program produces the expected results", "11-15 pts.\nWork contains most required elements\n   - programmer's name and the date\n   - comments explain the purpose of the program\n   - the program is correctly indented\n   - meaningful variable names are used\n   - the program runs correctly\n   - the program contains no syntax errors\n   - The program produces the expected results", "0-10 pts.\nWork contains few required elements\n   - programmer's name and the date\n   - comments explain the purpose of the program\n   - the program is correctly indented\n   - meaningful variable names are used\n   - the program runs correctly\n   - the program contains no syntax errors\n   - The program produces the expected results"],
          ["Program Content", "36-50 pts.\nWork contains all required elements\n   - Python code clearly reflects the pseudocode\n   - the main() function is properly declared\n   - at least on if, if-else, or elif statement is used\n   - at least one logical operator is used in a Boolean condition\n   - the input() function is used to prompt the user for at least three decision-making values\n   - results of inputs and decisions are clearly printed on screen", "21-35 pts.\nWork contains most required elements\n   - Python code clearly reflects the pseudocode\n   - the main() function is properly declared\n   - at least on if, if-else, or elif statement is used\n   - at least one logical operator is used in a Boolean condition\n   - the input() function is used to prompt the user for at least three decision-making values\n   - results of inputs and decisions are clearly printed on screen", "0-20 pts.\nWork contains few required elements\n   - Python code clearly reflects the pseudocode\n   - the main() function is properly declared\n   - at least on if, if-else, or elif statement is used\n   - at least one logical operator is used in a Boolean condition\n   - the input() function is used to prompt the user for at least three decision-making values\n   - results of inputs and decisions are clearly printed on screen"]]

def main():
    clr = "a"
    while True:
        if clr != "n":
            clear()
        choice = input("Do you want to:\nRun = Run the script and see if it works as intended\nCode = Look at the code to look for errors\nLeave = Stop grading the student's script\nRubric = Check the rubric\nGrade = Give the student a grade based on how they did in their code\nSelf = See what you're made of\nScripts = Choose a script from a list to see the code of it\n\n").lower()

        if choice == "run":
            with open(f"scripts/{script}") as f:
                code = f.read()

            exec(code)
            sleep(2)
            clear()
    
        elif choice == "code":
            a = 1
            with open(f"scripts/{script}") as f:
                for line in f:
                    print(f"{a}.   {line}", end= "")
                    a += 1
            clr = input("\nDo you want to clear the terminal? Y or N\n").lower()
            if clr == "y":
                clear()
            if clr == "n":
                print("\n")
    
        elif choice == "leave":
            clear()
            exit()
    
        elif choice == "rubric":
            print(tabulate(rubric, headers='firstrow', tablefmt='fancy_grid'))
            clr = input("\nDo you want to clear the terminal? Y or N\n").lower()
            if clr == "y":
                clear()
            if clr == "n":
                print("\n")

        elif choice == "grade":
            while True:
                print("Make sure to look at the rubric.")
                sure = input("Are you sure you want to grade the student's work? Y or N\n").lower()
                if sure == "n":
                    break
                try:
                    p1 = int(input("In numbers, how good did they do in Software Development Life Cycle? "))
                    p2 = int(input("In numbers, how good did they do in Program Design and Performance? "))
                    p3 = int(input("In numbers, how good did they do in Program Content? "))
                except ValueError:
                    print("You didn't input a number for one of the grades.")
                    sleep(2)
                    clear()
                else:
                    total = p1 + p2 + p3
                    print(f"\nYou gave the student a final score of {total}")
                    exit()

        elif choice == "self":
            a = 1
            with open("teacher.py") as f:
                for line in f:
                    print(f"{a}.   {line}", end= "")
                    a += 1
            clr = input("\nDo you want to clear the terminal? Y or N\n").lower()
            if clr == "y":
                clear()

        elif choice == "scripts":
            t = True
            while t is True:
                clear()
                num = 1
                print("Which script do you want? Type the full file name. It is case sensitive.")
                for name in scripts:
                    print(f"{num}.    {name}")
                    num += 1
                for name2 in readers:
                    print(f"{num}.    {name2}")
                    num += 1
                which = input(f"{num}.    ModePicker.py\nLeave\n")
                clear()
                if which in scripts:
                    a = 1
                    with open(f"scripts/{which}") as f:
                        for line in f:
                            print(f"{a}.   {line}", end= "")
                            a += 1
                    clr = input("\nDo you want to clear the terminal? Y or N\n").lower()
                    if clr == "y":
                        clear()
                elif which in readers:
                    a = 1
                    with open(which) as f:
                        for line in f:
                            print(f"{a}.   {line}", end= "")
                            a += 1
                    clr = input("\nDo you want to clear the terminal? Y or N\n").lower()
                    if clr == "y":
                        clear()
                elif which == "ModePicker.py":
                    a = 1
                    with open(which) as f:
                        for line in f:
                            print(f"{a}.   {line}", end= "")
                            a += 1
                    clr = input("\nDo you want to clear the terminal? Y or N\n").lower()
                    if clr == "y":
                        clear()
                elif which.lower() == "leave":
                    t = False
                    clear()
                else:
                    continue
        else:
            print("You didn't input one of the choices.")
main()