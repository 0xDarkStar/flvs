# Make a list of my favorite things from a category and have user say their favorite thing from that cat. If their input is in my list of favs, say that their answer is one of my favs too.

def main():
    ans = input("What is your favorite type of military ship? (general, not specific)\n\n").lower() # Get answer from user

    #Lists
    favs = ["aircraft carrier", "submarine", "corvette", "destroyer", "landing craft", "frigate"]
    vowels = ["a", "e", "i", "o", "u"]

    #Most of the work:
    a = 1
    if ans[0] in vowels:
        pre = "an"
    else: # This ^ checks if user's input starts with a vowel as to change the "a" to "an". If it doesn't, it uses "a".
        pre = "a"
    if ans[-1] == "s": # Checks if user put an "s" at the end of their answer (ex: corvettes). If they did put an "s" at the end, we get rid of it.
        ans = ans[0:-1]
    if ans in favs: # Checks if their answer is in the list of favs
        if ans in favs[0]: # Checks if their answer is first in list of favs
            print(f"\n{ans[0].upper()}{ans[1:50]}s are my favorite type of ship as well!\nAll of the types of ship I like are:")
        else: # It isn't? Then we change this          /----\
            print(f"\n{ans[0].upper()}{ans[1:50]}s are one of my favorite types of ship as well!\nAll of the types of ship I like are:")
    else: # Their answer wasn't in the list of favs
        print(f"\nI don't think I've heard of {pre} {ans} before. I'll have to check it out once I have the time.\nThe types of ships I do know of (and are my favorites) are: ")
    for i in favs: # Print out the list of favs
        print(f"    {a}. {i[0].upper()}{i[1:1000]}")
        a += 1

main()
