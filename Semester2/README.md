# Semester2

This module contains work used for the second semester

---

## [DND_Game](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/DND_Game)

A small DND game.

## [Encoding|Decoding](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/Encoding|Decoding)

Encodes any text into ASCII and decodes ASCII

1. [Encoder.py](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/Encoding|Decoding/Encoder.py)

## [FavList](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/FavList)

Checks if the user's favorite is in my list of favorites for a certain category

1. [FavList.py](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/FavList/FavList.py)

## [ProgCheck](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/ProgCheck)

User can grade scripts according to a rubric or fix scripts that have errors

1. [scripts](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/ProgCheck/scripts)
2. [ModePicker.py](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/ProgCheck/ModePicker.py)
3. [proofreader.py](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/ProgCheck/proofreader.py)
4. [teacher.py](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/ProgCheck/teacher.py)

## [TurtleGame](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/TurtleGame)

A maze game where the user traverses two mazes

1. [PathFind](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/TurtleGame/PathFind)
2. [__pycache__](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/TurtleGame/__pycache__)
3. [lvl1.py](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/TurtleGame/lvl1.py)
4. [lvl2.py](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/TurtleGame/lvl2.py)
5. [turtleGame.py](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/TurtleGame/turtleGame.py)
