# Encoding|Decoding

Encodes any text into ASCII and decodes ASCII

---

## [Encoder.py](https://github.com/0xDarkStar/FLVS/tree/main/Semester2/Encoding|Decoding/Encoder.py)

Uses the "random", "os", and "platform" modules. Allows the user to choose to get a quote and an encoded version of it in ASCII. Also allows the user to encode their own text into ASCII or decode some ASCII.
