# This program allows you to encode any text and decode any text in ASCII (wow, so cool...)

QuotesL = ['"Eighty percent of success is showing up." - Woody Allen', '"Hell is other people." - Jean-Paul Sartre', '"If you are going through hell, keep going." - Winston Churchill', '"Not all those who wander are lost." - J.R.R. Tolkein', '"Speak softly and carry a big stick." - Theodore Roosevelt', '"Nothing is certain except for death and taxes." - Benjamin Franklin', '"Parting is such sweet sorrow." - William Shakespeare', '"Three can keep a secret, if two of them are dead." - Benjamin Franklin']

# Encode = ord()
# Decode = chr()

import random

# Stand Alone mode

# By: 0xDarkStar

def clear():
    import platform, os
    OS = platform.system()
    if OS == "Linux" or OS == "macOs": # Find the operating system to be able to clear their terminal.
        os.system("clear")
    elif OS == "Windows":
        os.system("cls")
    else:
        print("What system are you using???")

illegals = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "'", '"', ".", "/", "?", ";", ":", "{","}", "\\", "|", "+", "=", "-", "_", "(", ")", "*", "&", "^", "%", "$", "#", "@", "!", "~", "`", "<", ">", " ", "\x1b"]

end = len(QuotesL)
quotes = random.randint(0, end - 1)

''' These are ANSI btw
Bold:
    \x1B[1m any text here would be bold \x1B[0m

Italics:
    \x1B[3m any text here would be italicized \x1B[0m

Underline:
    \x1B[4m any text here would be underlined \x1B[0m

Multiple:
    \x1B[1;3m any text here would be bold and italicized \x1B[0m

I use all of these in the code
'''

def main():
    clear()
    print("This program can encode any text into ASCII and decode any text in ASCII.") # Tell them what it does simply
    
    which = input("Do you want to receive a \x1B[1;4mquote\x1B[0m, do your \x1B[1;4mown\x1B[0m text, or \x1B[1;4mexit\x1B[0m the program?\n").lower() # What do they want to use?
    clear()

    if which == "quote": # They chose to get a quote
        EncQuote = "["
        quote = QuotesL[quotes]
        EncQuote = [ord(char) for char in quote] # It encodes the quote
        print(f"The encoded quote is:\n{EncQuote}") # And prints it
        print(f"\nThe decoded quote is:\n{quote}") # Then prints the original quote

    elif which == "own":
        which = input("Do you want to \x1B[1;4mencode\x1B[0m or \x1B[1;4mdecode\x1B[0m a message?\n") # They chose to use their own code/text. Now they choose to encode a message or decode one.
        clear()

        if which == "encode":
            encode = input("Input the message that you wish to encode.\n") # They can type whatever they want
            encode = [ord(char) for char in encode] # The code encodes it
            print(f"You're encoded message is:\n{encode}") # And returns it

        elif which == "decode": 
            decode = input("Input the code that you want to decode. \x1B[3mtype it like: [49, 57, etc.]\x1B[0m\n").lower()
            # They should follow the style thats shown but the code only needs the "," to tell that theres a new symbol, letter, or number
            pos = 0
            for i in range(len(decode)): # Looks through the string for any illegals (letters or symbols other than "[", "]", and ",")
                try:
                    if len(decode) == 0: # If there is nothing left of the string after sanitizing it, it stops the loop and goes past everything (because there's nothing to do) to line 112
                        break
                    elif pos not in range(len(decode)): # To try to stop the code from recieving an IndexError but still working
                        break
                    elif decode[pos] not in illegals: # That space in the string doesn't have any illegals, move on to the next one
                        pos += 1
                    elif decode[pos] in illegals: # There's an illegal here
                        decode = decode.replace(decode[pos], "") # It looks at where it is and deletes it. If it was left alone, it would break the code
                except IndexError:
                    print("Something went wrong. Please write a comment on my Github describing what you input for the error to occur.")
                    break # ^
            
            decode = decode.replace(" ", "") # Get rid of spaces

            while ",," in decode or "[[" in decode or "]]" in decode or "[," in decode or ",]" in decode or ",[" in decode or "]," in decode:
                decode = decode.replace(",,", ",") # Gets rid of all double commas as to not break it
                decode = decode.replace("[[", "[") # Gets rid of all double brackets
                decode = decode.replace("]]", "]")
                decode = decode.replace(",]", "]") # Gets rid of commas that have nothing before or after them
                decode = decode.replace("[,", "[")
                decode = decode.replace(",[", "[")
                decode = decode.replace("],", "]")

            decode = decode.replace("[", "") # Finally gets rid of the brackets, they were just used to help with the commas
            decode = decode.replace("]", "")
            if len(decode) != 0:
                try:
                    decode = list(map(int, decode.split(","))) # Turns string into list, splitting it at each ",". That's why there's all the code above
                    clear()
                    print(f"The encoded message is:\n{decode}")
                    decode = "".join([chr(char) for char in decode]) # Decodes the list
                    print(f"The decoded message is:\n{decode}")
                except ValueError:
                    print(f"Something went wrong, I wasn't able to decode this: {decode}")
                    print("Please try to follow the style that's shown to you next time.")
                except OverflowError:
                    print(f"Something went wrong, I wasn't able to decode this: {decode}")
                    print("You might've input a number too big to be decoded.")
            else:
                print("There was nothing to decode from ASCII in that message")
    
    elif which == "exit":
        clear()
        exit()

    else:
        main()




main()
