# A file with everything I might want to use in several projects


def clear():
    import platform, os
    OS = platform.system()
    if OS == "Linux" or OS == "Darwin": # Find the operating system to be able to clear their terminal.
        os.system("clear")
    elif OS == "Windows":
        os.system("cls")
    else:
        print("What system are you using???")


# Let
def addLine(script, lineNum, scriptCode, location):
    with open(f"{location}{script}", "r") as file:
        lines = file.readlines()
    
    line = input(f"\n\nAdding new line:\n")
    scriptCode.insert(lineNum, line)
    lines.insert(lineNum, line + "\n")
    
    with open(f"{location}{script}", "w") as file:
        file.writelines(lines)



def editLine(script, lineNum, scriptCode, location):
    with open(f"{location}{script}", "r") as file:
        lines = file.readlines()
    
    print("Make sure to retype any part that you want to keep.")
    line = input(f"\n\nEditing line {lineNum}:\n{scriptCode[lineNum-1]}")
    scriptCode[lineNum-1] = line
    lines[lineNum-1] = line + "\n"
    
    with open(f"{location}{script}", "w") as file:
        file.writelines(lines)